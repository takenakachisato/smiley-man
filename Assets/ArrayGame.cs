﻿using UnityEngine;

public class ArrayGame : MonoBehaviour {

    // どこからでもアクセスできる
    public int[] nums;

	void Start () {
		// 1. numsの中身を全部表示
        for (var i = 0; i < nums.Length; i++)
        {
            Debug.Log(nums[i]);
        }

        // 2. numsの全合計を表示
        int sum = 0;
        for (var i = 0; i < nums.Length; i++)
        {
            sum += nums[i];
        }
        Debug.Log(sum);
	}
}