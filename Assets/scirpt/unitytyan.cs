﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class unitytyan : MonoBehaviour {
    public Transform target;
    public Animator animator;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {
    agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        animator = GetComponent<Animator>();
        if (agent.enabled == true)
        {
            animator.SetBool ("walk",true);
            agent.SetDestination(target.position); 
        }
        else { animator.SetBool ("walk",false); }
	}
}
