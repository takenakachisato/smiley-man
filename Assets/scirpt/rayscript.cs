﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class rayscript : MonoBehaviour {
    public NavMeshAgent enemy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        shot();
	}
    void shot(){
        int distance = 5;
        Vector3 center = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        Ray ray = Camera.main.ScreenPointToRay(center);
        RaycastHit hitInfo;

        if(Physics.Raycast(ray,out hitInfo, distance))
        {
            Debug.DrawLine(ray.origin, hitInfo.point, Color.red);
            if (hitInfo.collider.CompareTag("enemy"))
            {
                enemy.enabled = false;

            }
            else
            {
                enemy.enabled = true;
            }  
        }
        else {
            enemy.enabled = true;
        }
    }
}
