﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayJudge : MonoBehaviour {
    public int[] nums;

    private void Start()
    {
        int odd = 0;
        int even = 0;
        for(var i = 0;i < nums.Length; i++)
        {
            if(nums[i]%2 == 0)
            {
                even++;
            }
            else
            {
                odd++;
            }
        }
        // nums配列の偶数と奇数の個数を数える！
        // (Hint)for文とif文を上手く使えばできるよ！
        // for文はArrayGame.cs
        // if文はMathGame.cs
        // を参考にしてみよう！
        Debug.Log("偶数は" + even + "個");
        Debug.Log("奇数は" + odd + "個");
    }
}
