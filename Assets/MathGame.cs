﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathGame : MonoBehaviour {

    public int A;

    public int B;

	void Update () {
        // A x Bが偶数ならDebug.Log("偶数");
        // A x Bが奇数ならDebug.Log("奇数");
        // と表示するプログラムを作ってみよう！！

        string result = "";
        if ((A * B) % 2 == 0)
        {
            result = "偶数";
        } else
        {
            result = "奇数";
        }

        // C#の機能を使えばこんな書き方もできる
        //string result = ((A * B) % 2 == 0) ? "偶数" : "奇数";
        Debug.Log(result);
	}
}
